﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.DAL;
using Microsoft.AspNet.Identity;
using ReferralApp.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using ReferralApp.Entities;

namespace ReferralApp.MigrationsApplicationDbContext
{
    public class Seeder
    {
        private static class RoleConstants
        {
            public const string Admin = "Admin";
            public const string Marketer = "Marketer";
            public const string Consumer = "Consumer";
        }

        public static void Seed(ApplicationDbContext context)
        {
          //  SeedRoles(context);
            SeedUsers(context);
        }

        private static void SeedRoles(ApplicationDbContext context)
        {
            int roleId = 1;
            foreach (var field in typeof(RoleConstants).GetFields())
            {
                var roleTitle = field.GetValue(null).ToString();

                if (!context.Roles.Any(r => r.Name.ToUpper() == roleTitle.ToUpper()))
                {
                    var store = new RoleStore<IdentityRole>(context);
                    var manager = new RoleManager<IdentityRole>(store);
                    var identityRole = new IdentityRole { Id = roleId.ToString(), Name = roleTitle };

                    manager.Create(identityRole);
                }
                roleId++;
            }
        }

        private static void SeedUsers(ApplicationDbContext context)
        {
            var userPassword = "P@ssword123";
            for (int userCounter = 1; userCounter <= 100; userCounter++)
            {
                var userEmail = string.Format("user{0}@email.com", userCounter.ToString());

                if (!context.Users.Any(u => u.Email == userEmail))
                {
                    var store = new UserStore<ApplicationUser>(context);
                    var manager = new UserManager<ApplicationUser>(store);
                    var PasswordHash = new PasswordHasher();
                    var user = new ApplicationUser
                    {
                        Id = userCounter.ToString(),
                        UserName = userEmail.Split('@')[0],
                        Email = userEmail,
                        PasswordHash = PasswordHash.HashPassword(userPassword),
                        LockoutEnabled = true,
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                        LastLoginTime = DateTime.UtcNow
                    };

                    manager.Create(user);
                    //manager.AddToRole(user.Id, RoleConstants.Marketer);
                    //manager.AddToRole(user.Id, RoleConstants.Consumer);
                }
            }
        }
    }
}