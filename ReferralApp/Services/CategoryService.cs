﻿using ReferralApp.DAL;
using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Services
{
    public class CategoryService
    {
        ReferralAppContext db = new ReferralAppContext();

        public IQueryable<Category> GetAllCategories()
        {
            var categories = db.Categories;
            return categories;
        }

        public Category GetCategoryByName(string name)
        {
            var category = db.Categories.SingleOrDefault(o => o.Name.ToUpper() == name.ToUpper());
            return category;
        }
    }
}