﻿using System;
using System.Collections.Generic;
using System.Linq;
using ReferralApp.Entities;
using System.Web;
using ReferralApp.DAL;
using System.Data.Entity;
using ReferralApp.Models;
using ReferralApp.Helpers;

namespace ReferralApp.Services
{
    public class ReferralListService
    {
        private TagService _tagService;
        private TagReferralListRelationshipService _tagReferralListRelationshipService;

        public List<ReferralList> GetAllActive()
        {
            using (var db = new ReferralAppContext())
            {
                List<ReferralList> referralLists = new List<ReferralList>();
                referralLists = db.ReferralLists
                    .Include(o => o.ReferralListRelationships)
                    .Include(o => o.TagReferralListRelationships.Select(tr => tr.Tag))
                    .Include(o => o.CreatedBy)
                    .Where(o => o.Archived == false)
                    .OrderByDescending(o => o.DateUpdated).ToList();
                return referralLists;
            }   
        }

        public void CreateReferralList(ReferralList referralList)
        {
            using (var db = new ReferralAppContext())
            {
                db.ReferralLists.Add(referralList);
                db.SaveChanges();
            }
        }

        public void CreateReferralList(CreateReferralListViewModel viewModel, string listTitle, string userID)
        {
            using (var db = new ReferralAppContext())
            {
                viewModel.ReferralList = new ReferralList();
                viewModel.ReferralList.Title = listTitle;
                viewModel.ReferralList.UserIdCreatedBy = userID;
                db.ReferralLists.Add(viewModel.ReferralList);
                db.SaveChanges();
            }
        }

        public List<ReferralList> GetReferralListsByUserIdCreatedBy(string userId, string sort = "")
        {
            using (var db = new ReferralAppContext())
            {
                var referralLists = db.ReferralLists.Where(o => o.UserIdCreatedBy == userId)
                    .Include(l => l.ReferralListAnalytics)
                    .Include(l => l.CreatedBy)
                    .Include(l => l.ReferralListRelationships.Select(relationship => relationship.Referral))
                    .Include(l => l.TagReferralListRelationships.Select(relationship => relationship.Tag));
                referralLists = Sort(sort, referralLists);
                return referralLists.ToList();
            }
        }

        public ReferralList GetReferralListById(int id)
        {
            var db = new ReferralAppContext();
            return db.ReferralLists
                .Include(l => l.ReferralListRelationships.Select(relationship => relationship.Referral.CreatedBy))
                .Include(l => l.TagReferralListRelationships.Select(relationship => relationship.Tag))
                .SingleOrDefault(l => l.ReferralListID == id);
            
        }

        public List<ReferralList> GetUserFeedReferralLists()
        {
            //TODO - fix to use using db context
            var db = new ReferralAppContext();
            List<ReferralList> lists = new List<ReferralList>();
            var user = ApplicationUserHelper.GetCurrentUser();
            var tags = db.TagFollowerRelationships
                    .Where(o => o.FollowerID.Equals(user.Id))
                    .Select(o => o.Tag);

            foreach (var t in tags)
            {
                var tagReferralLists =
                    t.TagReferralListRelationships
                    .Where(o => o.ReferralList.Archived != true && o.ReferralList.UserIdCreatedBy != user.Id)
                    .Select(o => o.ReferralList).AsQueryable()
                    .Include(o => o.ReferralListRelationships.Select(r => r.Referral))
                    .Include(o => o.CreatedBy);
                lists.AddRange(tagReferralLists);
            }

            var leaders = db.LeaderFollowerRelationships
                    .Include(o => o.UserLeader.ReferralLists)
                    .Where(o => o.FollowerID.Equals(user.Id))
                    .Select(o => o.UserLeader);

            foreach (var leader in leaders)
            {
                var referralListsOfLeader = leader.ReferralLists.Where(o => o.Archived != true);
                lists.AddRange(referralListsOfLeader);
            }
            var savedReferralList = db.ReferralListFollowerRelationships
                    .Include(o => o.ReferralList.CreatedBy)
                    .Include(o => o.ReferralList.TagReferralListRelationships.Select(tr => tr.Tag.Name))
                    .Include(o => o.ReferralList.ReferralListRelationships.Select(r => r.Referral))
                    .Where(o => o.FollowerID.Equals(user.Id) && o.ReferralList.Archived != true)
                    .Select(o => o.ReferralList);
            lists.AddRange(savedReferralList);
            lists = lists.Distinct().OrderByDescending(o => o.DateUpdated).ToList();
            return lists;
        }

        public void AddTags(ReferralList list, string tagNames)
        {
            try
            {
                _tagService = new TagService();
                _tagReferralListRelationshipService = new TagReferralListRelationshipService();
                var tagNamesSplit = tagNames.Split(',');
                for (int tagIndex = 0; tagIndex < tagNamesSplit.Length; tagIndex++)
                {
                    var tagName = tagNamesSplit[tagIndex];
                    var tag = _tagService.GetTagByName(tagName);
                    if (tag == null)
                    {
                        tag = _tagService.CreateTag(tagName);
                    }
                    _tagReferralListRelationshipService.Create(list.ReferralListID, tag.TagID);
                }
            }
            catch (Exception ex)
            { }
        }

        public IQueryable<ReferralList> Sort(string sort, IQueryable<ReferralList> lists)
        {
            try
            {
                switch (sort)
                {
                    case "popular":
                        // need to implement
                        lists = lists.OrderBy(r => r.DateCreated);
                        break;
                    case "date_newest":
                        lists = lists.OrderByDescending(r => r.DateCreated);
                        break;
                    case "date_oldest":
                        lists = lists.OrderBy(r => r.DateCreated);
                        break;
                    default:
                        lists = lists.OrderByDescending(r => r.DateCreated);
                        break;
                }
            }
            catch (Exception ex)
            {

            }
            return lists;
        }
    }
}