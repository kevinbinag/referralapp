﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ReferralApp.Entities;
using ReferralApp.DAL;

namespace ReferralApp.Services
{
    public class ReferralListRelationshipService
    {
        public void Create(string sReferralID, string sReferralListID)
        {
            using (var db = new ReferralAppContext())
            {
                int iReferralID = 0;
                int iReferralListID = 0;
                if (Int32.TryParse(sReferralID, out iReferralID) && Int32.TryParse(sReferralListID, out iReferralListID))
                {
                    var referralListRelationship = new ReferralListRelationship { ReferralID = iReferralID, ReferralListID = iReferralListID };
                    db.ReferralListRelationships.Add(referralListRelationship);
                    db.SaveChanges();
                }
            }
        }

        public void Delete(string sReferralID, string sReferralListID)
        {
            using (var db = new ReferralAppContext())
            {
                int iReferralID = 0;
                int iReferralListID = 0;
                if (Int32.TryParse(sReferralID, out iReferralID) && Int32.TryParse(sReferralListID, out iReferralListID))
                {
                    var referralListRelationship = db.ReferralListRelationships
                    .SingleOrDefault(o => o.ReferralID.Equals(iReferralID) && o.ReferralListID.Equals(iReferralListID));
                    db.ReferralListRelationships.Remove(referralListRelationship);
                    db.SaveChanges();
                }
            }
        }

        public List<ReferralListRelationship> GetRelationshipsByReferraListUserCreated(string profileOwnerID)
        {
            using (var db = new ReferralAppContext())
            {
                return db.ReferralListRelationships
                        .Include(o => o.Referral.TagReferralRelationships.Select(tr => tr.Tag))
                        .Include(o => o.Referral.CreatedBy)
                        .Where(o => o.ReferralList.UserIdCreatedBy == profileOwnerID)
                        .ToList();
            }
        }
    }
}