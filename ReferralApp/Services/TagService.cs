﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.DAL;
using ReferralApp.Entities;

namespace ReferralApp.Services
{
    public class TagService
    {
        public Tag CreateTag(string tagName = null)
        {
            using (var db = new ReferralAppContext())
            {
                var tag = new Tag();
                tag.Name = tagName;
                db.Tags.Add(tag);
                db.SaveChanges();
                return tag;
            }
        }
        public List<Tag> GetAllTags()
        {
            using (var db = new ReferralAppContext())
            {
                return db.Tags.ToList();
            }
        }

        public Tag GetTagByName(string name)
        {
            using (var db = new ReferralAppContext())
            {
                return db.Tags.SingleOrDefault(o => o.Name.ToUpper() == name.ToUpper());
            }
        }

        public List<Referral> GetTagReferrals(int tagID)
        {
            //using (var db = new ReferralAppContext())
            //{
            //    return db.TagReferralRelationships.Where(o => o.TagID == tagID).Select(o => o.Referral).ToList();
            //}   

            var db = new ReferralAppContext();
                return db.TagReferralRelationships.Where(o => o.TagID == tagID).Select(o => o.Referral).ToList();
            
        }
    }
}