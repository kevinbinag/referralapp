﻿using ReferralApp.DAL;
using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Services
{
    public class TagReferralRelationshipService
    {
        public void Create(int referralID, int tagID)
        {
            using (var db = new ReferralAppContext())
            {
                var tagReferralRelationship = new TagReferralRelationship { ReferralID = referralID, TagID = tagID };
                db.TagReferralRelationships.Add(tagReferralRelationship);
                db.SaveChanges();
            }
        }
    }
}