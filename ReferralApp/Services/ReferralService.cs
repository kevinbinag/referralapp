﻿using ReferralApp.DAL;
using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using ReferralApp.Helpers;

namespace ReferralApp.Services
{
    public class ReferralService
    {
        private TagService _tagService;
        private TagReferralRelationshipService _tagReferralRelationshipService;
        private ReferralListRelationshipService _referralListRelationshipService;

        public List<Referral> GetAllActive()
        {
            using (var db = new ReferralAppContext())
            {
                List<Referral> referrals = new List<Referral>();
                referrals = db.Referrals
                    .Include(o => o.CreatedBy)
                    .Include(o => o.TagReferralRelationships.Select(tr => tr.Tag))
                    .Where(o => o.Archived == false)
                    .OrderByDescending(o => o.DateUpdated).ToList();
                return referrals;
            }
        }
        public void Archive(int referralID)
        {
            using (var db = new ReferralAppContext())
            {
                var referral = db.Referrals.SingleOrDefault(o => o.ReferralID == referralID);
                referral.Archived = true;
                db.SaveChanges();
            }
        }
        public void Save(Referral referral)
        {
            using (var db = new ReferralAppContext())
            {
                db.Referrals.Add(referral);
                db.SaveChanges();
            }
        }

        public Referral GetReferralById(string referralID)
        {
            using (var db = new ReferralAppContext())
            {
                return db.Referrals.SingleOrDefault(o => o.ReferralID.ToString() == referralID);
            } 
        }

        public List<Referral> GetReferralsByUserIdCreatedBy(string userId, string sort)
        {
            using (var db = new ReferralAppContext())
            {
                var referrals = db.Referrals
                    .Include(o => o.CreatedBy)
                    .Include(o => o.ReferralAnalytics)
                    .Include(o => o.TagReferralRelationships.Select(tr => tr.Tag))
                    .Where(o => o.UserIdCreatedBy == userId);
                referrals = sort.Contains("archived") ? referrals.Where(o => o.ExpirationDate < DateTime.UtcNow || o.Archived)
                    : referrals.Where(o => o.Archived == false && (o.ExpirationDate >= DateTime.UtcNow || o.ExpirationDate == null));
                referrals = Sort(sort, referrals);
                return referrals.ToList();
            }
        }

        public List<Referral> GetUserProfileFollowingReferrals(string profileOwnerID)
        {
            using (var db = new ReferralAppContext())
            {
                List<Referral> followingReferrals = new List<Referral>();
                _referralListRelationshipService = new ReferralListRelationshipService();
                var userProfileOwnerRerralListRelationships = _referralListRelationshipService.GetRelationshipsByReferraListUserCreated(profileOwnerID);
                foreach (var relationship in userProfileOwnerRerralListRelationships)
                {
                    if (!relationship.Referral.UserIdCreatedBy.Equals(profileOwnerID))
                    {
                        if (!followingReferrals.Any(o => o.ReferralID == relationship.ReferralID))
                        {
                            followingReferrals.Add(relationship.Referral);
                        }
                    }
                }
                return followingReferrals.OrderByDescending(o => o.DateCreated).ToList();
            }
        }

        public List<Referral> GetUserFeedReferrals()
        {
            var db = new ReferralAppContext();
            List<Referral> referrals = new List<Referral>();
            var user = ApplicationUserHelper.GetCurrentUser();
            /*
            This tab will show referrals from: people that a user follow
tags that a user follow
referrals that a user relist
            */

            var tags = db.TagFollowerRelationships
                .Where(o => o.FollowerID.Equals(user.Id))
                .Select(o => o.Tag);

            foreach (var t in tags)
            {
                var tagReferrals =
                    t.TagReferralRelationships
                    .Where(o => o.Referral.Archived != true && o.Referral.UserIdCreatedBy != user.Id)
                    .Select(o => o.Referral).AsQueryable()
                    .Include(o => o.CreatedBy)
                    .Include(o => o.ExpirationDate);
                referrals.AddRange(tagReferrals);
            }

            var leaders = db.LeaderFollowerRelationships
                    .Include(o => o.UserLeader.Referrals)
                    .Where(o => o.FollowerID.Equals(user.Id))
                    .Select(o => o.UserLeader);

            foreach (var leader in leaders)
            {
                var referralsOfLeader = leader.Referrals.Where(o => o.Archived != true);
                referrals.AddRange(referralsOfLeader);
            }
            referrals = referrals.Distinct().OrderByDescending(o => o.DateUpdated).ToList();
            return referrals;
        }

        public IQueryable<Referral> Sort(string sort, IQueryable<Referral> referrals)
        {
            try
            {
                //referrals = GetUserReferralsById()
                switch (sort)
                {
                    case "popular":
                        // need to implement
                        referrals = referrals.OrderBy(r => r.DateCreated);
                        break;
                    case "date_newest":
                        referrals = referrals.OrderByDescending(r => r.DateCreated);
                        break;
                    case "date_oldest":
                        referrals = referrals.OrderBy(r => r.DateCreated);
                        break;
                    default:
                        referrals = referrals.OrderByDescending(r => r.DateCreated);
                        break;
                }
            }
            catch (Exception ex)
            {

            }
            return referrals;
        }

        public void AddTags(Referral referral, string tagNames)
        {
            try
            {
                _tagService = new TagService();
                _tagReferralRelationshipService = new TagReferralRelationshipService();
                var tagNamesSplit = tagNames.Split(',');
                for (int tagIndex = 0; tagIndex < tagNamesSplit.Length; tagIndex++)
                {
                    var tagName = tagNamesSplit[tagIndex];
                    var tag = _tagService.GetTagByName(tagName);
                    if (tag == null)
                    {
                        tag = _tagService.CreateTag(tagName);
                    }
                    _tagReferralRelationshipService.Create(referral.ReferralID, tag.TagID);
                }
            }
            catch (Exception ex)
            { }
        }
    }
}