﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.DAL;
using ReferralApp.Helpers;
using ReferralApp.Entities;
using System.IO;

namespace ReferralApp.Services
{
    public class FilePathService
    {
        private const string IMAGE_PATH = @"C:\Users\kevin_000\Desktop\Projects\ReferralApp\ReferralApp\Content\Images";

        public void Create(HttpPostedFileBase file)
        {
            using (var db = new ReferralAppContext())
            {
                var currentUser = ApplicationUserHelper.GetCurrentUser();
                var userProfilePhoto = new FilePath
                {
                    FileName = Path.GetFileName(file.FileName),
                    FileType = FileType.UserProfilePhoto
                };
                var filepathOwner = db.ApplicationUsers.SingleOrDefault(o => o.Id.Equals(currentUser.Id));
                filepathOwner.FilePaths.Add(userProfilePhoto);
                //TODO System.Web.HttpContext.Current.Server.MapPath
                file.SaveAs(Path.Combine(IMAGE_PATH, file.FileName));
                db.SaveChanges();
            }
        }

        public void Update(int filepathID, HttpPostedFileBase file)
        {
            using (var db = new ReferralAppContext())
            {
                var ofilepath = db.FilePaths.SingleOrDefault(o => o.FilePathID.Equals(filepathID));
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                file.SaveAs(Path.Combine(IMAGE_PATH, fileName));
                ofilepath.FileName = fileName;
                db.SaveChanges();
            }
        }

        public bool UserProfilePhotoExists()
        {
            using (var db = new ReferralAppContext())
            {
                var currentUser = ApplicationUserHelper.GetCurrentUser();
                var exists = db.FilePaths.Any(o => o.ApplicationUserID.Equals(currentUser.Id) && o.FileType.Equals(0));
                return exists;
            }
        }
    }
}