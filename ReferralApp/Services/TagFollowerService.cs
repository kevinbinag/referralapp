﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.DAL;
using ReferralApp.Entities;
using ReferralApp.Helpers;
using System.Data.Entity;

namespace ReferralApp.Services
{
    public class TagFollowerService
    {
        public void Create(int iTagID)
        {
            using (var db = new ReferralAppContext())
            {
                var currentUserID = ApplicationUserHelper.GetCurrentUserId();
                var relationship = new TagFollowerRelationship { TagID = iTagID, FollowerID = currentUserID };
                db.TagFollowerRelationships.Add(relationship);
                db.SaveChanges();
            }
        }

        public void Delete(int iTagID)
        {
            using (var db = new ReferralAppContext())
            {
                var currentUserID = ApplicationUserHelper.GetCurrentUserId();
                var relationship = db.TagFollowerRelationships.SingleOrDefault(o => o.TagID.Equals(iTagID) && o.FollowerID.Equals(currentUserID));
                db.TagFollowerRelationships.Remove(relationship);
                db.SaveChanges();
            }
        }

        public bool Exists(int iTagID)
        {
            using (var db = new ReferralAppContext())
            {
                bool exists = false;
                var currentUser = ApplicationUserHelper.GetCurrentUser();
                if(currentUser != null)
                    exists = db.TagFollowerRelationships.Any(o => o.TagID.Equals(iTagID) && o.FollowerID.Equals(currentUser.Id));
                return exists;
            }
        }

        public List<TagFollowerRelationship> GetRelationshipsByFollowerID(string userID)
        {
            using (var db = new ReferralAppContext())
            {
                return db.TagFollowerRelationships
                    .Include(o => o.Tag)
                    .Where(o => o.FollowerID.Equals(userID)).ToList();
            }
        }

        public IEnumerable<Tag> GetTagsFollowed(string userID)
        {
            using (var db = new ReferralAppContext())
            {
                return db.TagFollowerRelationships
                    .Where(o => o.FollowerID.Equals(userID))
                    .Select(o => o.Tag).ToList();
            }
        }

        public List<ReferralList> GetTagReferralListsFollowed(string userID)
        {
            using (var db = new ReferralAppContext())
            {
                List<ReferralList> referralLists = new List<ReferralList>();
                //var tags = db.TagFollowerRelationships
                //    .Include(o => o.Tag.TagReferralListRelationships
                //        .Select(x => x.ReferralList.ReferralListRelationships.Select(y => y.Referral)))
                //    .Where(o => o.FollowerID.Equals(userID))
                //    .Select(o => o.Tag);
                var tags = db.TagFollowerRelationships
                    .Where(o => o.FollowerID.Equals(userID))
                    .Select(o => o.Tag);

                
                foreach (var t in tags)
                {
                    var tagReferralLists = 
                        t.TagReferralListRelationships
                        .Select(o => o.ReferralList).AsQueryable();
                    var foo =
                        tagReferralLists
                        .Include(o => o.ReferralListRelationships)
                        .Include(o => o.ReferralListRelationships.Select(r => r.Referral))
                        .Include(o => o.CreatedBy).ToList();
                    //var tagReferralLists = t.TagReferralListRelationships.Select(o => o.ReferralList).AsQueryable();
                    //tagReferralLists = tagReferralLists
                    //    //.Include(o => o.ReferralListRelationships)
                    //    .Include(o => o.ReferralListRelationships.Select(x => x.Referral))
                    //    .Include(o => o.CreatedBy);
                    referralLists.AddRange(foo);
                }
                return referralLists;
            }
        }
    }
}