﻿using ReferralApp.DAL;
using ReferralApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Entities;
using ReferralApp.Models;
using System.Data.Entity;

namespace ReferralApp.Services
{
    public class UserService
    {
        public List<ApplicationUser> GetAllActive()
        {
            using (var db = new ReferralAppContext())
            {
                return db.ApplicationUsers.Include(o => o.FilePaths).ToList();
            }
        }
        public ApplicationUser GetUserProfileOwnerFromUrl(HttpRequestBase request)
        {
            string[] urlPathAndQuerySplit = request.Url.PathAndQuery.ToString().Split('/');
            var urlUsername = urlPathAndQuerySplit[1];
            var user = ApplicationUserHelper.GetUserByUserName(urlUsername);
            return user;
        }
    }
}