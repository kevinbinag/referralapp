﻿using ReferralApp.DAL;
using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Services
{
    public class TagReferralListRelationshipService
    {
        public void Create(int referralListID, int tagID)
        {
            using (var db = new ReferralAppContext())
            {
                var tagReferralListRelationship = new TagReferralListRelationship { ReferralListID = referralListID, TagID = tagID };
                db.TagReferralListRelationships.Add(tagReferralListRelationship);
                db.SaveChanges();
            }
        }
    }
}