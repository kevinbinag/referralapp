﻿using ReferralApp.DAL;
using ReferralApp.Entities;
using ReferralApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ReferralApp.Services
{
    public class LeaderFollowerRelationshipService
    {
        public void CreateLeaderFollowerRelationship(string userNameToFollow)
        {
            using (var db = new ReferralAppContext())
            {
                var currentUser = ApplicationUserHelper.GetCurrentUser();
                var userToFollow = ApplicationUserHelper.GetUserByUserName(userNameToFollow);
                var leaderFollowerRelationship = new LeaderFollowerRelationship { LeaderID = userToFollow.Id, FollowerID = currentUser.Id };
                db.LeaderFollowerRelationships.Add(leaderFollowerRelationship);
                db.SaveChanges();
            }
        }

        public void DeleteLeaderFollowerRelationship(string userNameToUnfollow)
        {
            using (var db = new ReferralAppContext())
            {
                var currentUser = ApplicationUserHelper.GetCurrentUser();
                var userToUnfollow = ApplicationUserHelper.GetUserByUserName(userNameToUnfollow);
                var leaderFollowerRelationship = db.LeaderFollowerRelationships.SingleOrDefault(l => l.LeaderID.Equals(userToUnfollow.Id) && l.FollowerID.Equals(currentUser.Id));
                db.LeaderFollowerRelationships.Remove(leaderFollowerRelationship);
                db.SaveChanges();
            }
        }

        public bool CurrentUserIsFollowingUser(string profileOwnerId)
        {
            using (var db = new ReferralAppContext())
            {
                bool following = false;
                var currentUser = ApplicationUserHelper.GetCurrentUser();
                if (currentUser != null)
                {
                    following = db.LeaderFollowerRelationships.Any(l => l.LeaderID.Equals(profileOwnerId) && l.FollowerID.Equals(currentUser.Id));
                }
                return following;
            }
        }

        public List<LeaderFollowerRelationship> GetRelationshipsByFollowerID(string userFollowerID)
        {
            using (var db = new ReferralAppContext())
            {
                return db.LeaderFollowerRelationships
                    .Include(o => o.UserLeader.FilePaths)
                    .Where(o => o.FollowerID.Equals(userFollowerID)).ToList();
            }
        }
        public List<ApplicationUser> GetLeadersOfUser(string userFollowerID)
        {
            using (var db = new ReferralAppContext())
            {
                return db.LeaderFollowerRelationships
                    .Include(o => o.UserLeader.FilePaths)
                    .Where(o => o.FollowerID.Equals(userFollowerID))
                    .Select(o => o.UserLeader).ToList();
            }
        }
    }
}