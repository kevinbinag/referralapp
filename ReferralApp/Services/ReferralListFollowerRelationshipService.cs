﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ReferralApp.DAL;
using ReferralApp.Helpers;
using ReferralApp.Entities;

namespace ReferralApp.Services
{
    public class ReferralListFollowerRelationshipService
    {
        public void Create(string referralListID)
        {
            using (var db = new ReferralAppContext())
            {
                int iReferralListID = 0;
                if (Int32.TryParse(referralListID, out iReferralListID))
                {
                    var currentUserID = ApplicationUserHelper.GetCurrentUserId();
                    var relationship = new ReferralListFollowerRelationship() { ReferralListID = iReferralListID, FollowerID = currentUserID };
                    db.ReferralListFollowerRelationships.Add(relationship);
                    db.SaveChanges();
                }
            }
        }

        public void Delete(string referralListID)
        {
            using (var db = new ReferralAppContext())
            {
                int iReferralListID = 0;
                if (Int32.TryParse(referralListID, out iReferralListID))
                {
                    var currentUserID = ApplicationUserHelper.GetCurrentUserId();
                    var relationship = db.ReferralListFollowerRelationships.SingleOrDefault(o => o.ReferralListID.Equals(iReferralListID) && o.FollowerID.Equals(currentUserID));
                    db.ReferralListFollowerRelationships.Remove(relationship);
                    db.SaveChanges();
                }
            }
        }

        public bool Exists(int referralListID)
        {
            using (var db = new ReferralAppContext())
            {
                bool exists = false;
                var currentUserID = ApplicationUserHelper.GetCurrentUserId();
                exists = db.ReferralListFollowerRelationships.Any(o => o.ReferralListID.Equals(referralListID) && o.FollowerID.Equals(currentUserID));
                return exists;
            }
        }

        public List<ReferralListFollowerRelationship> GetRelationshipsByFollowerID(string userID)
        {
            using (var db = new ReferralAppContext())
            {
                return db.ReferralListFollowerRelationships
                    .Include(o => o.ReferralList.TagReferralListRelationships.Select(tr => tr.Tag))
                    .Include(o => o.ReferralList.CreatedBy)
                    .Include(o => o.ReferralList.ReferralListRelationships.Select(r => r.Referral))
                    .Where(o => o.FollowerID.Equals(userID)).ToList();
            }
        }

        public List<ReferralList> GetReferralListsFollowed(string userID)
        {
            using (var db = new ReferralAppContext())
            {
                return db.ReferralListFollowerRelationships
                    .Include(o => o.ReferralList.CreatedBy)
                    .Include(o => o.ReferralList.ReferralListRelationships.Select(r => r.Referral))
                    .Where(o => o.FollowerID.Equals(userID))
                    .Select(o => o.ReferralList).ToList();
            }
        }
    }
}