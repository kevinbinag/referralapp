namespace ReferralApp.MigrationsReferralAppContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RelationshipsForFollowingTagsAndReferralList : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReferralListFollowerRelationships",
                c => new
                    {
                        ReferralListID = c.Int(nullable: false),
                        FollowerID = c.String(nullable: false, maxLength: 128),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.ReferralListID, t.FollowerID })
                .ForeignKey("dbo.ReferralLists", t => t.ReferralListID, cascadeDelete: false)
                .ForeignKey("dbo.ApplicationUsers", t => t.FollowerID, cascadeDelete: false)
                .Index(t => t.ReferralListID)
                .Index(t => t.FollowerID);
            
            CreateTable(
                "dbo.TagFollowerRelationships",
                c => new
                    {
                        TagID = c.Int(nullable: false),
                        FollowerID = c.String(nullable: false, maxLength: 128),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.TagID, t.FollowerID })
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: false)
                .ForeignKey("dbo.ApplicationUsers", t => t.FollowerID, cascadeDelete: false)
                .Index(t => t.TagID)
                .Index(t => t.FollowerID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReferralListFollowerRelationships", "FollowerID", "dbo.ApplicationUsers");
            DropForeignKey("dbo.ReferralListFollowerRelationships", "ReferralListID", "dbo.ReferralLists");
            DropForeignKey("dbo.TagFollowerRelationships", "FollowerID", "dbo.ApplicationUsers");
            DropForeignKey("dbo.TagFollowerRelationships", "TagID", "dbo.Tags");
            DropIndex("dbo.TagFollowerRelationships", new[] { "FollowerID" });
            DropIndex("dbo.TagFollowerRelationships", new[] { "TagID" });
            DropIndex("dbo.ReferralListFollowerRelationships", new[] { "FollowerID" });
            DropIndex("dbo.ReferralListFollowerRelationships", new[] { "ReferralListID" });
            DropTable("dbo.TagFollowerRelationships");
            DropTable("dbo.ReferralListFollowerRelationships");
        }
    }
}
