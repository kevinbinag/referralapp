namespace ReferralApp.MigrationsReferralAppContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedUrlCodeFromReferralBase : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Referrals", "Url", c => c.String(nullable: false));
            DropColumn("dbo.ReferralLists", "Url");
            DropColumn("dbo.ReferralLists", "Code");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ReferralLists", "Code", c => c.String());
            AddColumn("dbo.ReferralLists", "Url", c => c.String());
            AlterColumn("dbo.Referrals", "Url", c => c.String());
        }
    }
}
