namespace ReferralApp.MigrationsReferralAppContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Analytics : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReferralAnalytics",
                c => new
                    {
                        ReferralAnalyticsID = c.Int(nullable: false, identity: true),
                        ReferralID = c.Int(nullable: false),
                        AnalyticsType = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ReferralAnalyticsID)
                .ForeignKey("dbo.Referrals", t => t.ReferralID, cascadeDelete: true)
                .Index(t => t.ReferralID);
            
            CreateTable(
                "dbo.ReferralListAnalytics",
                c => new
                    {
                        ReferralListAnalyticsID = c.Int(nullable: false, identity: true),
                        ReferralListID = c.Int(nullable: false),
                        AnalyticsType = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ReferralListAnalyticsID)
                .ForeignKey("dbo.ReferralLists", t => t.ReferralListID, cascadeDelete: true)
                .Index(t => t.ReferralListID);
            
            DropColumn("dbo.ReferralLists", "Impressions");
            DropColumn("dbo.ReferralLists", "Clicks");
            DropColumn("dbo.ReferralLists", "Saves");
            DropColumn("dbo.Referrals", "Impressions");
            DropColumn("dbo.Referrals", "Clicks");
            DropColumn("dbo.Referrals", "Saves");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Referrals", "Saves", c => c.Int(nullable: false));
            AddColumn("dbo.Referrals", "Clicks", c => c.Int(nullable: false));
            AddColumn("dbo.Referrals", "Impressions", c => c.Int(nullable: false));
            AddColumn("dbo.ReferralLists", "Saves", c => c.Int(nullable: false));
            AddColumn("dbo.ReferralLists", "Clicks", c => c.Int(nullable: false));
            AddColumn("dbo.ReferralLists", "Impressions", c => c.Int(nullable: false));
            DropForeignKey("dbo.ReferralListAnalytics", "ReferralListID", "dbo.ReferralLists");
            DropForeignKey("dbo.ReferralAnalytics", "ReferralID", "dbo.Referrals");
            DropIndex("dbo.ReferralListAnalytics", new[] { "ReferralListID" });
            DropIndex("dbo.ReferralAnalytics", new[] { "ReferralID" });
            DropTable("dbo.ReferralListAnalytics");
            DropTable("dbo.ReferralAnalytics");
        }
    }
}
