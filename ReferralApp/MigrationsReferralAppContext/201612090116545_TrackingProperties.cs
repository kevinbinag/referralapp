namespace ReferralApp.MigrationsReferralAppContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrackingProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReferralLists", "Impressions", c => c.Int(nullable: false));
            AddColumn("dbo.ReferralLists", "Clicks", c => c.Int(nullable: false));
            AddColumn("dbo.ReferralLists", "Saves", c => c.Int(nullable: false));
            AddColumn("dbo.Referrals", "Impressions", c => c.Int(nullable: false));
            AddColumn("dbo.Referrals", "Clicks", c => c.Int(nullable: false));
            AddColumn("dbo.Referrals", "Saves", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Referrals", "Saves");
            DropColumn("dbo.Referrals", "Clicks");
            DropColumn("dbo.Referrals", "Impressions");
            DropColumn("dbo.ReferralLists", "Saves");
            DropColumn("dbo.ReferralLists", "Clicks");
            DropColumn("dbo.ReferralLists", "Impressions");
        }
    }
}
