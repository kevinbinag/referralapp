namespace ReferralApp.MigrationsReferralAppContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _11062016_test0 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        LastLoginTime = c.DateTime(nullable: false),
                        ModelChangeTestOne = c.String(),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IdentityUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.LeaderFollowerRelationships",
                c => new
                    {
                        LeaderID = c.String(nullable: false, maxLength: 128),
                        FollowerID = c.String(nullable: false, maxLength: 128),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LeaderID, t.FollowerID })
                .ForeignKey("dbo.ApplicationUsers", t => t.FollowerID, cascadeDelete: false)
                .ForeignKey("dbo.ApplicationUsers", t => t.LeaderID, cascadeDelete: false)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.LeaderID)
                .Index(t => t.FollowerID)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.IdentityUserLogins",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.ReferralLists",
                c => new
                    {
                        ReferralListID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(unicode: false),
                        Url = c.String(),
                        Code = c.String(),
                        ExpirationDate = c.DateTime(),
                        Archived = c.Boolean(nullable: false),
                        UserIdCreatedBy = c.String(nullable: false, maxLength: 128),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ReferralListID)
                .ForeignKey("dbo.ApplicationUsers", t => t.UserIdCreatedBy, cascadeDelete: true)
                .Index(t => t.UserIdCreatedBy);
            
            CreateTable(
                "dbo.ReferralListRelationships",
                c => new
                    {
                        ReferralID = c.Int(nullable: false),
                        ReferralListID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.ReferralID, t.ReferralListID })
                .ForeignKey("dbo.Referrals", t => t.ReferralID, cascadeDelete: true)
                .ForeignKey("dbo.ReferralLists", t => t.ReferralListID, cascadeDelete: true)
                .Index(t => t.ReferralID)
                .Index(t => t.ReferralListID);
            
            CreateTable(
                "dbo.Referrals",
                c => new
                    {
                        ReferralID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(unicode: false),
                        Url = c.String(),
                        Code = c.String(),
                        ExpirationDate = c.DateTime(),
                        Archived = c.Boolean(nullable: false),
                        UserIdCreatedBy = c.String(nullable: false, maxLength: 128),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ReferralID)
                .ForeignKey("dbo.ApplicationUsers", t => t.UserIdCreatedBy, cascadeDelete: false)
                .Index(t => t.UserIdCreatedBy);
            
            CreateTable(
                "dbo.IdentityUserRoles",
                c => new
                    {
                        RoleId = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ApplicationUser_Id = c.String(maxLength: 128),
                        IdentityRole_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.RoleId, t.UserId })
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .ForeignKey("dbo.IdentityRoles", t => t.IdentityRole_Id)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.IdentityRole_Id);
            
            CreateTable(
                "dbo.IdentityRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IdentityUserRoles", "IdentityRole_Id", "dbo.IdentityRoles");
            DropForeignKey("dbo.IdentityUserRoles", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.ReferralListRelationships", "ReferralListID", "dbo.ReferralLists");
            DropForeignKey("dbo.ReferralListRelationships", "ReferralID", "dbo.Referrals");
            DropForeignKey("dbo.Referrals", "UserIdCreatedBy", "dbo.ApplicationUsers");
            DropForeignKey("dbo.ReferralLists", "UserIdCreatedBy", "dbo.ApplicationUsers");
            DropForeignKey("dbo.IdentityUserLogins", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.LeaderFollowerRelationships", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.LeaderFollowerRelationships", "LeaderID", "dbo.ApplicationUsers");
            DropForeignKey("dbo.LeaderFollowerRelationships", "FollowerID", "dbo.ApplicationUsers");
            DropForeignKey("dbo.IdentityUserClaims", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropIndex("dbo.IdentityUserRoles", new[] { "IdentityRole_Id" });
            DropIndex("dbo.IdentityUserRoles", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Referrals", new[] { "UserIdCreatedBy" });
            DropIndex("dbo.ReferralListRelationships", new[] { "ReferralListID" });
            DropIndex("dbo.ReferralListRelationships", new[] { "ReferralID" });
            DropIndex("dbo.ReferralLists", new[] { "UserIdCreatedBy" });
            DropIndex("dbo.IdentityUserLogins", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.LeaderFollowerRelationships", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.LeaderFollowerRelationships", new[] { "FollowerID" });
            DropIndex("dbo.LeaderFollowerRelationships", new[] { "LeaderID" });
            DropIndex("dbo.IdentityUserClaims", new[] { "ApplicationUser_Id" });
            DropTable("dbo.IdentityRoles");
            DropTable("dbo.IdentityUserRoles");
            DropTable("dbo.Referrals");
            DropTable("dbo.ReferralListRelationships");
            DropTable("dbo.ReferralLists");
            DropTable("dbo.IdentityUserLogins");
            DropTable("dbo.LeaderFollowerRelationships");
            DropTable("dbo.IdentityUserClaims");
            DropTable("dbo.ApplicationUsers");
        }
    }
}
