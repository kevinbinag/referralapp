namespace ReferralApp.MigrationsReferralAppContext
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class ReferralAppConfiguration : DbMigrationsConfiguration<ReferralApp.DAL.ReferralAppContext>
    {
        public ReferralAppConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"MigrationsReferralAppContext";
        }

        protected override void Seed(ReferralApp.DAL.ReferralAppContext context)
        {
            Seeder.Seed(context);
        }
    }
}
