namespace ReferralApp.MigrationsReferralAppContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _11072016_categories_tags : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategoryReferralListRelationships",
                c => new
                    {
                        ReferralListID = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.ReferralListID, t.CategoryID })
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.ReferralLists", t => t.ReferralListID, cascadeDelete: true)
                .Index(t => t.ReferralListID)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.CategoryReferralRelationships",
                c => new
                    {
                        ReferralID = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.ReferralID, t.CategoryID })
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.Referrals", t => t.ReferralID, cascadeDelete: true)
                .Index(t => t.ReferralID)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.TagReferralListRelationships",
                c => new
                    {
                        ReferralListID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.ReferralListID, t.TagID })
                .ForeignKey("dbo.ReferralLists", t => t.ReferralListID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.ReferralListID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.TagReferralRelationships",
                c => new
                    {
                        ReferralID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.ReferralID, t.TagID })
                .ForeignKey("dbo.Referrals", t => t.ReferralID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.ReferralID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagReferralRelationships", "TagID", "dbo.Tags");
            DropForeignKey("dbo.TagReferralRelationships", "ReferralID", "dbo.Referrals");
            DropForeignKey("dbo.TagReferralListRelationships", "TagID", "dbo.Tags");
            DropForeignKey("dbo.TagReferralListRelationships", "ReferralListID", "dbo.ReferralLists");
            DropForeignKey("dbo.CategoryReferralListRelationships", "ReferralListID", "dbo.ReferralLists");
            DropForeignKey("dbo.CategoryReferralListRelationships", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.CategoryReferralRelationships", "ReferralID", "dbo.Referrals");
            DropForeignKey("dbo.CategoryReferralRelationships", "CategoryID", "dbo.Categories");
            DropIndex("dbo.TagReferralRelationships", new[] { "TagID" });
            DropIndex("dbo.TagReferralRelationships", new[] { "ReferralID" });
            DropIndex("dbo.TagReferralListRelationships", new[] { "TagID" });
            DropIndex("dbo.TagReferralListRelationships", new[] { "ReferralListID" });
            DropIndex("dbo.CategoryReferralRelationships", new[] { "CategoryID" });
            DropIndex("dbo.CategoryReferralRelationships", new[] { "ReferralID" });
            DropIndex("dbo.CategoryReferralListRelationships", new[] { "CategoryID" });
            DropIndex("dbo.CategoryReferralListRelationships", new[] { "ReferralListID" });
            DropTable("dbo.TagReferralRelationships");
            DropTable("dbo.Tags");
            DropTable("dbo.TagReferralListRelationships");
            DropTable("dbo.CategoryReferralRelationships");
            DropTable("dbo.Categories");
            DropTable("dbo.CategoryReferralListRelationships");
        }
    }
}
