namespace ReferralApp.MigrationsReferralAppContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FilePaths : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FilePaths",
                c => new
                    {
                        FilePathID = c.Int(nullable: false, identity: true),
                        FileName = c.String(maxLength: 255),
                        FileType = c.Int(nullable: false),
                        ApplicationUserID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.FilePathID)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUserID)
                .Index(t => t.ApplicationUserID);
            
            DropColumn("dbo.ApplicationUsers", "ModelChangeTestOne");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ApplicationUsers", "ModelChangeTestOne", c => c.String());
            DropForeignKey("dbo.FilePaths", "ApplicationUserID", "dbo.ApplicationUsers");
            DropIndex("dbo.FilePaths", new[] { "ApplicationUserID" });
            DropTable("dbo.FilePaths");
        }
    }
}
