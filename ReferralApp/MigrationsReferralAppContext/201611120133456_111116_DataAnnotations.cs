namespace ReferralApp.MigrationsReferralAppContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _111116_DataAnnotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ReferralLists", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Referrals", "Title", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Referrals", "Title", c => c.String());
            AlterColumn("dbo.ReferralLists", "Title", c => c.String());
        }
    }
}
