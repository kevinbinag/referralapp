﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.DAL;
using System.Data.Entity.Migrations;
using ReferralApp.Entities;
using ReferralApp.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace ReferralApp.MigrationsReferralAppContext
{
    public class Seeder
    {
        public static void Seed(ReferralAppContext context)
        {
            SeedTags(context);
            SeedUsers(context);
            SeedCategories(context);
            SeedReferrals(context);
            SeedReferralLists(context);
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static void SeedUsers(ReferralAppContext context)
        {
            var userPassword = "P@ssword123";
            for (int userCounter = 1; userCounter <= 100; userCounter++)
            {
                var userEmail = string.Format("user{0}@email.com", userCounter.ToString());

                if (!context.ApplicationUsers.Any(u => u.Email == userEmail))
                {
                    var store = new UserStore<ApplicationUser>(context);
                    var manager = new UserManager<ApplicationUser>(store);
                    var PasswordHash = new PasswordHasher();
                    var user = new ApplicationUser
                    {
                        Id = userCounter.ToString(),
                        UserName = userEmail.Split('@')[0],
                        Email = userEmail,
                        PasswordHash = PasswordHash.HashPassword(userPassword),
                        LockoutEnabled = true,
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                        LastLoginTime = DateTime.UtcNow
                    };

                    manager.Create(user);
                    //manager.AddToRole(user.Id, RoleConstants.Marketer);
                    //manager.AddToRole(user.Id, RoleConstants.Consumer);
                }
            }
        }

        private static void SeedTags(ReferralAppContext context)
        {
            for (int tagCounter = 1; tagCounter <= 500; tagCounter++)
            {
                if (tagCounter < 126)
                {
                    context.Tags.AddOrUpdate(o => o.Name,
                        new Tag { TagID = tagCounter, Name = RandomString(10) }
                        );
                }
                else if (tagCounter > 125 && tagCounter < 151)
                {
                    context.Tags.AddOrUpdate(o => o.Name,
                        new Tag { TagID = tagCounter, Name = RandomString(9) }
                        );
                }
                else if (tagCounter > 150 && tagCounter < 176)
                {
                    context.Tags.AddOrUpdate(o => o.Name,
                        new Tag { TagID = tagCounter, Name = RandomString(8) }
                        );
                }
                else
                {
                    context.Tags.AddOrUpdate(o => o.Name,
                       new Tag { TagID = tagCounter, Name = RandomString(20) }
                       );
                }
                context.SaveChanges();
            }
        }

        private static void SeedCategories(ReferralAppContext context)
        {
            context.Categories.AddOrUpdate(c => new { c.CategoryId, c.Name} ,
                    new Category { CategoryId = 1, Name = "Art"},
                    new Category { CategoryId = 2, Name = "Automotive" },
                    new Category { CategoryId = 3, Name = "Clothing" },
                    new Category { CategoryId = 4, Name = "Office Supplies" },
                    new Category { CategoryId = 5, Name = "Party Supplies" },
                    new Category { CategoryId = 6, Name = "Travel" },
                    new Category { CategoryId = 7, Name = "Jewelry" },
                    new Category { CategoryId = 8, Name = "Food" },
                    new Category { CategoryId = 9, Name = "Pet" },
                    new Category { CategoryId = 10, Name = "Restaurants" },
                    new Category { CategoryId = 11, Name = "Beauty" },
                    new Category { CategoryId = 12, Name = "Craft Supplies" },
                    new Category { CategoryId = 13, Name = "Adult" },
                    new Category { CategoryId = 14, Name = "Home" },
                    new Category { CategoryId = 15, Name = "Sporting Goods" },
                    new Category { CategoryId = 16, Name = "Tools" },
                    new Category { CategoryId = 17, Name = "Health" },
                    new Category { CategoryId = 18, Name = "Electronics" },
                    new Category { CategoryId = 19, Name = "Education" },
                    new Category { CategoryId = 20, Name = "Events" }
                );
            context.SaveChanges();
        }

        private static void SeedReferrals(ReferralAppContext context)
        {
            //context.Referrals.AddOrUpdate(r => r.ReferralID,
            //    new Referral { ReferralID = 1, })
            var testTitle = "Test Title ";
            var testUrl = "http://youtube.com";
            var testDescription = "This is a referral description. This referral will save you money and make me money. Win-win";
            var testReferralCode = "TESTCODE ";
            var userOneID = "1";
            for (int referralCounter = 1; referralCounter <= 100; referralCounter++)
            {
                if (referralCounter < 26)
                {
                    //Affiliate links, no code
                    context.Referrals.AddOrUpdate(r => r.ReferralID,
                    new Referral
                    {
                        ReferralID = referralCounter,
                        Title = testTitle + referralCounter.ToString(),
                        Url = testUrl,
                        Description = testDescription,
                        ExpirationDate = DateTime.UtcNow.AddDays(7),
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                        UserIdCreatedBy = userOneID
                    });
                }
                else if (referralCounter > 25 && referralCounter < 51)
                {
                    // Referral codes
                    context.Referrals.AddOrUpdate(r => r.ReferralID,
                    new Referral
                    {
                        ReferralID = referralCounter,
                        Title = testTitle + referralCounter.ToString(),
                        Url = testUrl,
                        Code = testReferralCode + referralCounter.ToString(),
                        Description = testDescription,
                        ExpirationDate = DateTime.UtcNow.AddDays(30),
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                        UserIdCreatedBy = userOneID
                    });
                }
                else if (referralCounter > 50 && referralCounter < 76)
                {
                    // no expiration
                    context.Referrals.AddOrUpdate(r => r.ReferralID,
                    new Referral
                    {
                        ReferralID = referralCounter,
                        Title = testTitle + referralCounter.ToString(),
                        Url = testUrl,
                        Code = testReferralCode + referralCounter.ToString(),
                        Description = testDescription,
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                        UserIdCreatedBy = userOneID
                    });
                }
                else
                {
                    // Expired referrals
                    context.Referrals.AddOrUpdate(r => r.ReferralID,
                    new Referral
                    {
                        ReferralID = referralCounter,
                        Title = testTitle + referralCounter.ToString(),
                        Url = testUrl,
                        Code = testReferralCode + referralCounter.ToString(),
                        Description = testDescription,
                        ExpirationDate = DateTime.UtcNow.AddDays(-60),
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                        UserIdCreatedBy = userOneID
                    });
                }
                context.SaveChanges();
            }

            //for (int referralCounter = 1; referralCounter <= 100; referralCounter++)
            //{

            //}


        }

        private static void SeedReferralLists(ReferralAppContext context)
        {

        }
    }
}