﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ReferralApp.DAL;
using ReferralApp.Entities;
using ReferralApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReferralApp.Helpers
{
    public class ApplicationUserHelper
    {
        public static ApplicationUser GetCurrentUser()
        {
            ApplicationUser user = null;
            try
            {
                user = HttpContext.Current
                                  .GetOwinContext()
                                  .GetUserManager<ApplicationUserManager>()
                                  .FindById(HttpContext.Current.User.Identity.GetUserId());
            }
            catch (Exception ex)
            {

            }
            return user;
        }

        public static string GetCurrentUserId()
        {
            ApplicationUser user = null;
            try
            {
                user = HttpContext.Current
                                  .GetOwinContext()
                                  .GetUserManager<ApplicationUserManager>()
                                  .FindById(HttpContext.Current.User.Identity.GetUserId());
            }
            catch (Exception ex)
            {

            }
            return user.Id;
        }

        public static string GetCurrentUserEmail()
        {
            ApplicationUser user = null;
            try
            {
                user = HttpContext.Current
                                  .GetOwinContext()
                                  .GetUserManager<ApplicationUserManager>()
                                  .FindById(HttpContext.Current.User.Identity.GetUserId());
            }
            catch (Exception ex)
            {

            }
            return user.Email;
        }

        public static bool UserNameExists(string userName)
        {
            bool userExists = false;
            try
            {
                ApplicationUser user = HttpContext.Current
                                  .GetOwinContext()
                                  .GetUserManager<ApplicationUserManager>()
                                  .FindByName(userName);

                if (user != null)
                {
                    userExists = true;
                }
            }
            catch (Exception ex)
            {

            }
            return userExists;
        }

        public static bool UserEmailExists(string email)
        {
            bool userExists = false;
            try
            {
                ApplicationUser user = HttpContext.Current
                                  .GetOwinContext()
                                  .GetUserManager<ApplicationUserManager>()
                                  .FindByEmail(email);

                if (user != null)
                {
                    userExists = true;
                }
            }
            catch (Exception ex)
            {

            }
            return userExists;
        }

        public static ApplicationUser GetUserByUserName(string userName)
        {
            ApplicationUser user = null;
            try
            {
                user = HttpContext.Current
                                  .GetOwinContext()
                                  .GetUserManager<ApplicationUserManager>()
                                  .FindByName(userName);
            }
            catch (Exception ex)
            {

            }
            return user;
        }

        

    }
}