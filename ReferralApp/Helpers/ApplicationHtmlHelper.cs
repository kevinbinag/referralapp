﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReferralApp.Helpers
{
    public static class ApplicationHtmlHelper
    {
        public static string IsSelected(this HtmlHelper html, string controllers = "", string actions = "", string cssClass = "selected")
        {
            ViewContext viewContext = html.ViewContext;
            bool isChildAction = viewContext.Controller.ControllerContext.IsChildAction;

            if (isChildAction)
                viewContext = html.ViewContext.ParentActionViewContext;

            RouteValueDictionary routeValues = viewContext.RouteData.Values;
            string currentAction = routeValues["action"].ToString();
            string currentController = routeValues["controller"].ToString();

            if (String.IsNullOrEmpty(actions))
                actions = currentAction;

            if (String.IsNullOrEmpty(controllers))
                controllers = currentController;

            string[] acceptedActions = actions.Trim().Split(',').Distinct().ToArray();
            string[] acceptedControllers = controllers.Trim().Split(',').Distinct().ToArray();

            return acceptedActions.Contains(currentAction) && acceptedControllers.Contains(currentController) ?
                cssClass : String.Empty;
        }

        public static List<SortSelection> BuildSortSelection(string sortValue)
        {
            List<SortSelection> sortSelection = new List<SortSelection>()
            {
                new SortSelection() { Text = "Date added (newest)", Value = "date_newest", IsSelected = string.IsNullOrEmpty(sortValue) || sortValue == "date_newest" ? true : false },
                new SortSelection() { Text = "Date added (oldest)", Value = "date_oldest", IsSelected = sortValue == "date_oldest" ? true : false },
                new SortSelection() { Text = "Archived", Value = "archived", IsSelected = sortValue == "archived" ? true : false}
                //new SortSelection() { Text = "Popular", Value = "popular", IsSelected = sortValue == "popular" ? true : false }
            };
            return sortSelection;
        }
    }
}