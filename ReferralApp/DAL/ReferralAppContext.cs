﻿using Microsoft.AspNet.Identity.EntityFramework;
using ReferralApp.Entities;
using ReferralApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ReferralApp.DAL
{
    public class ReferralAppContext : DbContext
    {
        public ReferralAppContext() : base("ReferralAppContext")
        {
          
        }

        public DbSet<Referral> Referrals { get; set; }
        public DbSet<ReferralList> ReferralLists { get; set; }
        public DbSet<ReferralListRelationship> ReferralListRelationships { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<LeaderFollowerRelationship> LeaderFollowerRelationships { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryReferralRelationship> CategoryReferralRelationships { get; set; }
        public DbSet<CategoryReferralListRelationship> CategoryReferralListRelationships { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagReferralRelationship> TagReferralRelationships { get; set; }
        public DbSet<TagReferralListRelationship> TagReferralListRelationships { get; set; }
        public DbSet<TagFollowerRelationship> TagFollowerRelationships { get; set; }
        public DbSet<ReferralListFollowerRelationship> ReferralListFollowerRelationships { get; set; }
        public DbSet<FilePath> FilePaths { get; set; }
        public DbSet<ReferralAnalytics> ReferralAnalytics { get; set; }
        public DbSet<ReferralListAnalytics> ReferralListAnalytics { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUserLogin>().HasKey<string>(l => l.UserId);
            modelBuilder.Entity<IdentityRole>().HasKey<string>(r => r.Id);
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });

            modelBuilder.Entity<Referral>().HasKey(r => r.ReferralID)
                .HasRequired(r => r.CreatedBy).WithMany(r => r.Referrals).HasForeignKey(r => r.UserIdCreatedBy);
            modelBuilder.Entity<ReferralList>().HasKey(r => r.ReferralListID)
                .HasRequired(r => r.CreatedBy).WithMany(r => r.ReferralLists).HasForeignKey(r => r.UserIdCreatedBy);

            //modelBuilder.Entity<ApplicationUser>().HasMany(r => r.Referrals).WithRequired(r => r.CreatedBy).HasForeignKey(r => r.UserIdCreatedBy);
            //modelBuilder.Entity<ApplicationUser>().HasMany(r => r.ReferralLists).WithRequired(r => r.CreatedBy).HasForeignKey(r => r.UserIdCreatedBy);
            //modelBuilder.Entity<ApplicationUser>().ToTable("")
            base.OnModelCreating(modelBuilder);

        }

        public static ReferralAppContext Create()
        {
            return new ReferralAppContext();
        }
    }
}