﻿using Microsoft.AspNet.Identity.EntityFramework;
using ReferralApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Entities;
using System.Data.Entity;

namespace ReferralApp.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("ReferralAppContext", throwIfV1Schema: false)
        {
           
        }

      //  public DbSet<LeaderFollowerRelationship> LeaderFollowerRelationships { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
          
        //    base.OnModelCreating(modelBuilder);

        //}
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}