﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    
    public class ReferralAnalytics : BaseAnalytics
    {
        public int ReferralAnalyticsID { get; set; }

        public int ReferralID { get; set; }
        public virtual Referral Referral { get; set; }
    }
}