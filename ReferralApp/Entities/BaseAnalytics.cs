﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    public enum AnalyticsType
    {
        Impressions,
        Clicks,
        Saves
    }

    public abstract class BaseAnalytics : IAuditEntity
    {
        public BaseAnalytics()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }
        public AnalyticsType AnalyticsType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}