﻿using ReferralApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    public abstract class BaseReferral : IAuditEntity
    {
        public BaseReferral()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }

        public string UserIdCreatedBy { get; set; }
        [Required]
        public string Title { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string Description { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public bool Archived { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }

        public virtual ApplicationUser CreatedBy { get; set; }
    }
}