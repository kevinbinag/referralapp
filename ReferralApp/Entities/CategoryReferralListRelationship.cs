﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    public class CategoryReferralListRelationship : IAuditEntity
    {
        public CategoryReferralListRelationship()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }

        [Key, ForeignKey("ReferralList"), Column(Order = 0)]
        public int ReferralListID { get; set; }
        [Key, ForeignKey("Category"), Column(Order = 1)]
        public int CategoryID { get; set; }

        public virtual ReferralList ReferralList { get; set; }
        public virtual Category Category { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}