﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System;

namespace ReferralApp.Entities
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser, IAuditEntity
    {
        public ApplicationUser()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        //TODO:
       // public string UserProfileImageUrl { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public DateTime LastLoginTime { get; set; }

        public virtual ICollection<Referral> Referrals { get; set; }
        public virtual ICollection<ReferralList> ReferralLists { get; set; }
        public virtual ICollection<LeaderFollowerRelationship> LeaderFollowerRelationships { get; set; }
        public virtual ICollection<TagFollowerRelationship> TagFollowerRelationships { get; set; }
        public virtual ICollection<ReferralListFollowerRelationship> ReferralListFollowerRelationships { get; set; }
        public virtual ICollection<FilePath> FilePaths { get; set; }
    }
}