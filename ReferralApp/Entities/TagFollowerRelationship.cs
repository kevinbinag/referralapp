﻿using ReferralApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    public class TagFollowerRelationship : IAuditEntity
    {
        public TagFollowerRelationship()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }

        [Key, ForeignKey("Tag"), Column(Order = 0)]
        public int TagID { get; set; }
        [Key, ForeignKey("UserFollower"), Column(Order = 1)]
        public string FollowerID { get; set; }
        
        public virtual Tag Tag { get; set; }
        public virtual ApplicationUser UserFollower { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}