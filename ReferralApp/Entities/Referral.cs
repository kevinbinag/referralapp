﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ReferralApp.Entities
{
    public class Referral : BaseReferral
    {
        public int ReferralID { get; set; }
        [Required]
        public string Url { get; set; }
        public string Code { get; set; }
        public virtual ICollection<ReferralListRelationship> ReferralListRelationships { get; set; }
        public virtual ICollection<TagReferralRelationship> TagReferralRelationships { get; set; }
        public virtual ICollection<ReferralAnalytics> ReferralAnalytics { get; set; }
    }
}