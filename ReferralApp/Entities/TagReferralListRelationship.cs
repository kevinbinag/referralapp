﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    public class TagReferralListRelationship : IAuditEntity
    {
        public TagReferralListRelationship()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }

        [Key, ForeignKey("ReferralList"), Column(Order = 0)]
        public int ReferralListID { get; set; }
        [Key, ForeignKey("Tag"), Column(Order = 1)]
        public int TagID { get; set; }

        public virtual ReferralList ReferralList { get; set; }
        public virtual Tag Tag { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}