﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReferralApp.Entities
{
    public class LeaderFollowerRelationship : IAuditEntity
    {
        public LeaderFollowerRelationship()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }

        [Key, ForeignKey("UserLeader"), Column(Order = 0)]
        public string LeaderID { get; set; }
        [Key, ForeignKey("UserFollower"), Column(Order = 1)]
        public string FollowerID { get; set; }

        public virtual ApplicationUser UserLeader { get; set; }
        public virtual ApplicationUser UserFollower { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}