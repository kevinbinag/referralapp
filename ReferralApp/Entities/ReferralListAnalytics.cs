﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    public class ReferralListAnalytics : BaseAnalytics
    {
        public int ReferralListAnalyticsID { get; set; }
       
        public int ReferralListID { get; set; }
        public virtual ReferralList ReferralList { get; set; }
    }
}