﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<CategoryReferralRelationship> CategoryReferralRelationships { get; set; }
        public virtual ICollection<CategoryReferralListRelationship> CategoryReferralListRelationships { get; set; }
    }
}