﻿using ReferralApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    public class ReferralList : BaseReferral
    {
        public int ReferralListID { get; set; }
        public virtual ICollection<ReferralListRelationship> ReferralListRelationships { get; set; }
        public virtual ICollection<CategoryReferralListRelationship> CategoryReferralListRelationships { get; set; }
        public virtual ICollection<TagReferralListRelationship> TagReferralListRelationships { get; set; }
        public virtual ICollection<ReferralListFollowerRelationship> ReferralListFollowerRelationships { get; set; }
        public virtual ICollection<ReferralListAnalytics> ReferralListAnalytics { get; set; }
    }
}