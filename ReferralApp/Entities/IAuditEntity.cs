﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferralApp.Entities
{
    public interface IAuditEntity
    {
         DateTime DateCreated { get; set; }
         DateTime DateUpdated { get; set; }
    }
}
