﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReferralApp.Entities
{
    /// <summary>
    /// This is the entity model for the join table between Referral and ReferralList
    /// </summary>
    public class ReferralListRelationship : IAuditEntity
    {
        public ReferralListRelationship()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
        }

        [Key, ForeignKey("Referral"), Column(Order = 0)]
        public int ReferralID { get; set; }
        [Key, ForeignKey("ReferralList"), Column(Order = 1)]
        public int ReferralListID { get; set; }

        public virtual Referral Referral { get; set; }
        public virtual ReferralList ReferralList { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}