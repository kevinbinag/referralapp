﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    public class UserCustomLink : IAuditEntity
    {
        public int UserCustomLinkID { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}