﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Entities
{
    public class Tag
    {
        public int TagID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<TagReferralRelationship> TagReferralRelationships { get; set; }
        public virtual ICollection<TagReferralListRelationship> TagReferralListRelationships { get; set; }
        public virtual ICollection<TagFollowerRelationship> TagFollowerRelationships { get; set; }
    }
}