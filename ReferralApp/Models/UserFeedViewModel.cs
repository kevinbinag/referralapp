﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Entities;

namespace ReferralApp.Models
{
    public class UserFeedViewModel : BaseViewModel
    {
        public UserFeedReferralsViewModel UserFeedReferralsViewModel { get; set; }
        public UserFeedReferralListsViewModel UserFeedReferralListsViewModel { get; set; }
        public UserFeedFollowingViewModel UserFeedFollowingViewModel { get; set; }
    }

    public class UserFeedReferralsViewModel : BaseViewModel
    {
        public List<Referral> ReferralsFollowing { get; set; }
    }

    public class UserFeedReferralListsViewModel : BaseViewModel
    {
        public List<ReferralList> ReferralListsFollowing { get; set; }
    }

    public class UserFeedFollowingViewModel : BaseViewModel
    {
        public List<LeaderFollowerRelationship> LeaderFollowerRelationships { get; set; }
    }
}