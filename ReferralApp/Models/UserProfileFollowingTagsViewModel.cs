﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Entities;

namespace ReferralApp.Models
{
    public class UserProfileFollowingTagsViewModel : BaseUserProfileViewModel
    {
        public List<TagFollowerRelationship> TagFollowerRelationships { get; set; }
    }
}