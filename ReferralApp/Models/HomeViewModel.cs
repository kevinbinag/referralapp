﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Entities;

namespace ReferralApp.Models
{
    public class HomeViewModel
    {
        public List<Tag> Tags { get; set; }
        public List<ReferralList> ReferralLists { get; set; }
        public List<Referral> Referrals { get; set; }
        public List<ApplicationUser> PeopleToFollow { get; set; }
    }
}