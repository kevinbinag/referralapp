﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class ReferralListDetailsViewModel : BaseUserProfileViewModel
    {
        public ReferralList ReferralList { get; set; }
        public List<ReferralList> CurrentUserReferralLists { get; set; }
        public List<Referral> Referrals { get; set; }
        public bool ReferralListIsFollowedByCurrentUser { get; set; }
    }
}