﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class CreateReferralViewModel
    {
        public Referral Referral { get; set; }
        public List<string> TagNames { get; set; }
    }
}