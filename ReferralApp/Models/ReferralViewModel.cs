﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class ReferralViewModel
    {
        public Referral Referral { get; set; }
        public ReferralList ReferralList { get; set; }
        public List<Referral> Referrals { get; set; }
        public List<ReferralList> ReferralLists { get; set; }
        public List<ReferralListRelationship> ReferralListRelationship { get; set; }
    }
}