﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Entities;

namespace ReferralApp.Models
{
    public class ReferralListViewModel
    {
        public ReferralList ReferralList { get; set; }
        public List<ReferralList> ReferralLists { get; set; }
    }
}