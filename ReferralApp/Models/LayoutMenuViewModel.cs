﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class LayoutMenuViewModel
    {
        public List<Category> Categories { get; set; }
        public ApplicationUser CurrentUser { get; set; }
    }
}