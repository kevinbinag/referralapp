﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Entities;

namespace ReferralApp.Models
{
    public class UserProfileFollowingReferralListsViewModel : BaseUserProfileViewModel
    {
        public List<ReferralListFollowerRelationship> ReferralListFollowerRelationships { get; set; }
    }
}