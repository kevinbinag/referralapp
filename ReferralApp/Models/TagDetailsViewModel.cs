﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class TagDetailsViewModel
    {
        public Tag Tag { get; set; }
        public ReferralList ReferralList { get; set; }
        public List<Referral> TagReferrals { get; set; }
        public List<ReferralList> TagReferrallists { get; set; }
        public bool CurrentUserIsFollowingTag { get; set; }
    }
}