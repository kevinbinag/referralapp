﻿using ReferralApp.Entities;
using ReferralApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class BaseViewModel 
    {
       public string CurrentUserProfilePhotoFileName { get; set; }
    }
}