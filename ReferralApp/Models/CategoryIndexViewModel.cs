﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class CategoryIndexViewModel
    {
        public List<Category> Categories { get; set; }
        public List<string> CategoryNames { get; set; }
    }
}