﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Entities;

namespace ReferralApp.Models
{
    public class AllFeedReferralsViewModel
    {
        public List<Referral> Referrals { get; set; }
    }
}