﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class BaseUserProfileViewModel
    {
        public ApplicationUser UserProfileOwner { get; set; }
        public bool FollowingUser { get; set; }
        public string UserProfilePhotoFileName { get; set; }
    }
}