﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Entities;

namespace ReferralApp.Models
{
    public class UserProfileFollowingReferralsViewModel : BaseUserProfileViewModel
    {
        //Referrals added to list but user does not own (liked or added to)
        public ReferralList ReferralList { get; set; }
        public List<Referral> UserProfileFollowingReferrals { get; set; }
    }
}