﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class ReferralDetailsViewModel : BaseUserProfileViewModel
    {
        public Referral Referral { get; set; }
    }
}