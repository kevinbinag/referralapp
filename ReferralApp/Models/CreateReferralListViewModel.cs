﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class CreateReferralListViewModel
    {
        public ReferralList ReferralList { get; set; }
    }
}