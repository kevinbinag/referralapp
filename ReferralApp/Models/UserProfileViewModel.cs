﻿using ReferralApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReferralApp.Models
{
    public class UserProfileViewModel : BaseUserProfileViewModel
    {
        public ReferralList ReferralList { get; set; }
        public List<Referral> Referrals { get; set; }
        public List<ReferralList> ReferralLists { get; set; }
        public bool NoReferralsLeft { get; set; }
        public bool NoReferralListsLeft { get; set; }
        public SortSelection SortSelectionSelected { get; set; }
        public List<SortSelection> SortSelectionsNotSelected { get; set; }
    }
}