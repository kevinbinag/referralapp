﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReferralApp.Entities;

namespace ReferralApp.Models
{
    public class UserProfileFollowingPeopleViewModel : BaseUserProfileViewModel
    {
        public List<LeaderFollowerRelationship> LeaderFollowerRelationships { get; set; }
    }
}