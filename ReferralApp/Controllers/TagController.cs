﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReferralApp.Models;
using ReferralApp.Services;

namespace ReferralApp.Controllers
{
    public class TagController : BaseController
    {
        private TagService _tagService;
        private ReferralService _referralService;
        private ReferralListService _referralListService;
        private TagFollowerService _tagFollowerService;

        public TagController()
        {
            _tagService = new TagService();
            _referralService = new ReferralService();
            _referralListService = new ReferralListService();
            _tagFollowerService = new TagFollowerService();
        }
        // GET: Tag
        public ActionResult Index()
        {
            TagIndexViewModel model = new TagIndexViewModel();
            model.Tags = _tagService.GetAllTags();
            return View(model);
        }

        public ActionResult TagDetails(string tagName)
        {
            TagDetailsViewModel model = new TagDetailsViewModel();
            model.Tag = _tagService.GetTagByName(tagName);
            model.TagReferrals = _tagService.GetTagReferrals(model.Tag.TagID);
            model.CurrentUserIsFollowingTag = _tagFollowerService.Exists(model.Tag.TagID);
            return View(model);
        }

        public ActionResult GetTagNames()
        {
            var tagNames = _tagService.GetAllTags().OrderBy(o => o.Name).Select(o => o.Name);
            return Json(new { tagNames = tagNames }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FollowTag(int tagID)
        {
            _tagFollowerService.Create(tagID);
            return Json(new object { });
        }

        [HttpPost]
        public ActionResult UnfollowTag(int tagID)
        {
            _tagFollowerService.Delete(tagID);
            return Json(new object { });
        }
    }
}