﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReferralApp.Models;
using ReferralApp.Services;
using ReferralApp.Helpers;

namespace ReferralApp.Controllers
{
    public class UserAnalyticsController : BaseController
    {
        private ReferralService _referralService;
        private ReferralListService _referralListService;

        public UserAnalyticsController()
        {
            _referralService = new ReferralService();
            _referralListService = new ReferralListService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserAnalyticsReferrals()
        {
            UserAnalyticsReferralsViewModel viewModel = new UserAnalyticsReferralsViewModel();
            var userID = ApplicationUserHelper.GetCurrentUserId();
            //TODO separate archived
            viewModel.Referrals = _referralService.GetReferralsByUserIdCreatedBy(userID, string.Empty);
            return View(viewModel);
        }

        public ActionResult UserAnalyticsReferralLists()
        {
            UserAnalyticsReferralListsViewModel viewModel = new UserAnalyticsReferralListsViewModel();
            var userID = ApplicationUserHelper.GetCurrentUserId();
            //TODO separate archived
            viewModel.ReferralLists = _referralListService.GetReferralListsByUserIdCreatedBy(userID, string.Empty);
            return View(viewModel);
        }
    }
}