﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReferralApp.DAL;
using ReferralApp.Models;

using ReferralApp.Helpers;
using ReferralApp.Services;

namespace ReferralApp.Controllers
{
    public class ReferralListController : BaseController
    {
        private ReferralService _referralService;
        private ReferralListService _referralListService;
        private ReferralListRelationshipService _referralListRelationshipService;
        private ReferralListFollowerRelationshipService _referralListFollowerRelationshipService;

        public ReferralListController()
        {
            _referralService = new ReferralService();
            _referralListService = new ReferralListService();
            _referralListRelationshipService = new ReferralListRelationshipService();
            _referralListFollowerRelationshipService = new ReferralListFollowerRelationshipService();
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(CreateReferralListViewModel viewModel, string referralID, string tags, string listTitle)
        {
            var user = ApplicationUserHelper.GetCurrentUser();
            //CreateReferralListViewModel viewModel = new CreateReferralListViewModel();
            try
            {
                if (ModelState.IsValid)
                {
                    if (viewModel.ReferralList == null)
                    {
                        //viewModel = new CreateReferralListViewModel();
                        //viewModel.ReferralList = new ReferralList();
                        //viewModel.ReferralList.Title = listTitle;
                        _referralListService.CreateReferralList(viewModel, listTitle, user.Id);
                    }
                    else
                    {
                        viewModel.ReferralList.UserIdCreatedBy = user.Id;
                        _referralListService.CreateReferralList(viewModel.ReferralList);
                    }
                    
                    
                    if (!string.IsNullOrEmpty(referralID))
                    {
                        _referralListRelationshipService.Create(referralID, viewModel.ReferralList.ReferralListID.ToString());
                        return Redirect(Request.UrlReferrer.LocalPath);
                    }
                    if (!string.IsNullOrEmpty(tags))
                    {
                        _referralListService.AddTags(viewModel.ReferralList, tags);
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return Redirect(string.Format("/{0}/lists/{1}", user.UserName, viewModel.ReferralList.ReferralListID));
        }

        [HttpPost]
        public ActionResult AddReferralToList(string referralID, string referralListID)
        {
            _referralListRelationshipService.Create(referralID, referralListID);
            return Json(new object { });
        }

        [HttpPost]
        public ActionResult RemoveReferralFromList(string referralID, string referralListID)
        {
            _referralListRelationshipService.Delete(referralID, referralListID);
            return Json(new object { });
        }

        public ActionResult GetReferralListsCurrentUser(string referralID)
        {
            ReferralListsAddToViewModel viewModel = new ReferralListsAddToViewModel();
            viewModel.Referral = _referralService.GetReferralById(referralID);
            if (Request.IsAuthenticated)
            {
                viewModel.ReferralLists = _referralListService.GetReferralListsByUserIdCreatedBy(ApplicationUserHelper.GetCurrentUserId().ToString()).ToList();
            }
            return Json(new { partialHtml = RenderRazorViewToString("~/Views/Shared/_ReferralListsAddTo.cshtml", viewModel) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveReferralList(string referralListID)
        {
            _referralListFollowerRelationshipService.Create(referralListID);
            return Json(new object { });
        }

        [HttpPost]
        public ActionResult UnsaveReferralList(string referralListID)
        {
            _referralListFollowerRelationshipService.Delete(referralListID);
            return Json(new object { });
        }
    }
}