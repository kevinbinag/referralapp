﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReferralApp.Models;
using ReferralApp.Helpers;
using ReferralApp.DAL;
using ReferralApp.Services;
using System.IO;

namespace ReferralApp.Controllers
{
    public class UserProfileController : BaseController
    {
        private UserService _userService;
        private LeaderFollowerRelationshipService _leaderFollowerRelationshipService;
        private ReferralService _referralService;
        private ReferralListService _referralListService;
        private ReferralListFollowerRelationshipService _referralListFollowerRelationshipService;
        private TagFollowerService _tagFollowerService;

        public UserProfileController()
        {
            _userService = new UserService();
            _leaderFollowerRelationshipService = new LeaderFollowerRelationshipService();
            _referralService = new ReferralService();
            _referralListService = new ReferralListService();
            _referralListFollowerRelationshipService = new ReferralListFollowerRelationshipService();
            _tagFollowerService = new TagFollowerService();
        }

        public ActionResult UserProfileHome(UserProfileViewModel viewModel, string userName)
        {
            try
            {
                SetBaseUserProfile(viewModel);
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }

        public ActionResult UserProfileReferrals(UserProfileViewModel viewModel, string sort)
        {
            try
            {
                SetBaseUserProfile(viewModel);
                var sortSelection = ApplicationHtmlHelper.BuildSortSelection(sort);
                viewModel.SortSelectionSelected = sortSelection.SingleOrDefault(s => s.IsSelected);
                viewModel.SortSelectionsNotSelected = sortSelection.Where(s => !s.IsSelected).ToList();
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }

        public ActionResult UserProfileReferralLists(UserProfileViewModel viewModel, string sort)
        {
            try
            {
                SetBaseUserProfile(viewModel);
                var sortSelection = ApplicationHtmlHelper.BuildSortSelection(sort);
                viewModel.SortSelectionSelected = sortSelection.SingleOrDefault(s => s.IsSelected);
                viewModel.SortSelectionsNotSelected = sortSelection.Where(s => !s.IsSelected).ToList();
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }

        public ActionResult UserProfileFollowing(UserProfileFollowingReferralsViewModel viewModel)
        {
            try
            {
                SetBaseUserProfile(viewModel);
                viewModel.UserProfileFollowingReferrals = _referralService.GetUserProfileFollowingReferrals(viewModel.UserProfileOwner.Id);
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }

        public ActionResult UserProfileFollowingReferralLists(UserProfileFollowingReferralListsViewModel viewModel)
        {
            try
            {
                SetBaseUserProfile(viewModel);
                viewModel.ReferralListFollowerRelationships = _referralListFollowerRelationshipService.GetRelationshipsByFollowerID(viewModel.UserProfileOwner.Id);
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }

        public ActionResult UserProfileFollowingTags(UserProfileFollowingTagsViewModel viewModel)
        {
            try
            {
                SetBaseUserProfile(viewModel);
                viewModel.TagFollowerRelationships = _tagFollowerService.GetRelationshipsByFollowerID(viewModel.UserProfileOwner.Id);
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }

        public ActionResult UserProfileFollowingPeople(UserProfileFollowingPeopleViewModel viewModel)
        {
            try
            {
                SetBaseUserProfile(viewModel);
                viewModel.LeaderFollowerRelationships = _leaderFollowerRelationshipService.GetRelationshipsByFollowerID(viewModel.UserProfileOwner.Id);
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }

        public ActionResult UserProfileReferralDetails(int referralId)
        {
            ReferralDetailsViewModel viewModel = new ReferralDetailsViewModel();
            try
            {
                SetBaseUserProfile(viewModel);
                viewModel.Referral = _referralService.GetReferralById(referralId.ToString());
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }

        public ActionResult UserProfileListDetails(int listId)
        {
            ReferralListDetailsViewModel viewModel = new ReferralListDetailsViewModel();
            try
            {
                SetBaseUserProfile(viewModel);
                viewModel.ReferralList = _referralListService.GetReferralListById(listId);
                viewModel.ReferralListIsFollowedByCurrentUser = _referralListFollowerRelationshipService.Exists(viewModel.ReferralList.ReferralListID);
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }

        public ActionResult UserProfileAbout(UserProfileViewModel viewModel)
        {
            try
            {
                SetBaseUserProfile(viewModel);
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult UserProfileFollow(string userName)
        {
            try
            {
                _leaderFollowerRelationshipService.CreateLeaderFollowerRelationship(userName);
                return Json(new { success = true, responseFoo = string.Format("Followed {0} succesfully", userName), JsonRequestBehavior.AllowGet });
            }
            catch (Exception ex)
            {

            }
            return View();
        }

        [HttpPost]
        public ActionResult UserProfileUnfollow(string userName)
        {
            try
            {
                _leaderFollowerRelationshipService.DeleteLeaderFollowerRelationship(userName);
                return Json(new { success = true, responseFoo = string.Format("Unfollowed {0} succesfully", userName), JsonRequestBehavior.AllowGet });
            }
            catch (Exception ex)
            {

            }
            return View();
        }

        public ActionResult LoadReferrals(int pageIndex, int pageSize, string userName, UserProfileViewModel viewModel, string sort)
        {
            viewModel.UserProfileOwner = ApplicationUserHelper.GetUserByUserName(userName);
            var nextPageIndex = pageIndex + 1;
            var userProfileReferrals = _referralService.GetReferralsByUserIdCreatedBy(viewModel.UserProfileOwner.Id, sort);
            var referralCountToLoadNextPage = userProfileReferrals.Skip(nextPageIndex * pageSize)
                         .Take(pageSize).Count();
            viewModel.Referrals = userProfileReferrals.Skip(pageIndex * pageSize)
                         .Take(pageSize).ToList();
            if (referralCountToLoadNextPage <= 0)
            {
                viewModel.NoReferralsLeft = true;
            }
            return Json(
                new
                {
                    noReferralsLeft = viewModel.NoReferralsLeft,
                    partialHtml = RenderRazorViewToString("~/Views/UserProfile/_ReferralsToLoad.cshtml", viewModel)
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadReferralLists(int pageIndex, int pageSize, string userName, UserProfileViewModel viewModel, string sort)
        {
            viewModel.UserProfileOwner = ApplicationUserHelper.GetUserByUserName(userName);
            var nextPageIndex = pageIndex + 1;
            var referralLists = _referralListService.GetReferralListsByUserIdCreatedBy(viewModel.UserProfileOwner.Id, sort);
            var referralListsCountToLoadNextPage = referralLists.Skip(nextPageIndex * pageSize)
                         .Take(pageSize).Count();
            viewModel.ReferralLists = referralLists.Skip(pageIndex * pageSize)
                         .Take(pageSize)
                         .ToList(); 
            if (referralListsCountToLoadNextPage <= 0)
            {
                viewModel.NoReferralListsLeft = true;
            }
            return Json(
            new
            {
                noReferralListsLeft = viewModel.NoReferralListsLeft,
                partialHtml = RenderRazorViewToString("~/Views/UserProfile/_ReferralListsToLoad.cshtml", viewModel)
            },
            JsonRequestBehavior.AllowGet);
        }

        #region Methods
        private void SetBaseUserProfile(BaseUserProfileViewModel viewModel)
        {
            viewModel.UserProfileOwner = _userService.GetUserProfileOwnerFromUrl(Request);
            viewModel.FollowingUser = _leaderFollowerRelationshipService.CurrentUserIsFollowingUser(viewModel.UserProfileOwner.Id);
            var profilePhoto = viewModel.UserProfileOwner.FilePaths.FirstOrDefault(o => o.FileType == Entities.FileType.UserProfilePhoto);
            if (profilePhoto != null)
            {
                if (!System.IO.File.Exists(Path.Combine(@"C:\Users\kevin_000\Desktop\Projects\ReferralApp\ReferralApp\Content\Images", profilePhoto.FileName)))
                    profilePhoto = null;
            }
            viewModel.UserProfilePhotoFileName = profilePhoto != null ? profilePhoto.FileName : "";
        }
        #endregion
    }
}