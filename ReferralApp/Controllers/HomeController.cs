﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReferralApp.Models;
using ReferralApp.Services;

namespace ReferralApp.Controllers
{
    public class HomeController : BaseController
    {
        private TagService _tagService;
        private ReferralListService _referralListService;
        private ReferralService _referralService;
        private UserService _userService;

        public HomeController()
        {
            _tagService = new TagService();
            _referralListService = new ReferralListService();
            _referralService = new ReferralService();
            _userService = new UserService();
        }
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "UserFeed");
            }
            HomeViewModel viewModel = new HomeViewModel();
            viewModel.Tags = _tagService.GetAllTags().Take(50).ToList();
            viewModel.ReferralLists = _referralListService.GetAllActive().Take(10).ToList();
            viewModel.Referrals = _referralService.GetAllActive().Take(10).ToList();
            viewModel.PeopleToFollow = _userService.GetAllActive().Take(10).ToList();
            return View(viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}