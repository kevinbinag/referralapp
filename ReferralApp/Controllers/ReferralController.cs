﻿using ReferralApp.DAL;
using ReferralApp.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using ReferralApp.Helpers;
using ReferralApp.Services;

namespace ReferralApp.Controllers
{
    public class ReferralController : BaseController
    {
        private ReferralService _referralService;
       
        public ReferralController()
        {
            _referralService = new ReferralService();
        }

        [Authorize]
        public ActionResult Create()
        {
            CreateReferralViewModel viewmodel = new CreateReferralViewModel();
            return View(viewmodel);
        }

        //TODO: add tags input for (cateogry and tags)
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateReferralViewModel viewModel, string tags)
        {
            var user = ApplicationUserHelper.GetCurrentUser();
            try
            {
                //http://stackoverflow.com/questions/15333513/why-modelstate-isvalid-always-return-false-in-mvc
                //var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    viewModel.Referral.UserIdCreatedBy = user.Id;
                    _referralService.Save(viewModel.Referral);
                    if (!string.IsNullOrEmpty(tags))
                    {
                        _referralService.AddTags(viewModel.Referral, tags);
                    }
                    return Redirect("/" + user.UserName);
                }
            }
            catch (Exception ex)
            {
                return View("Error");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Archive(int referralID)
        {
            try
            {
                _referralService.Archive(referralID);
                return Json(new { });
            }
            catch (Exception ex)
            {

            }
            return View();
        }
    }
}