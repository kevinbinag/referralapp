﻿using ReferralApp.Models;
using ReferralApp.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReferralApp.Helpers;

namespace ReferralApp.Controllers
{
    public class BaseController : Controller
    {
        public LayoutMenuViewModel LayoutMenuViewModel;

        public BaseController()
        {
            LayoutMenuViewModel = new LayoutMenuViewModel();
            var user = ApplicationUserHelper.GetCurrentUser();
            if(user != null)
                LayoutMenuViewModel.CurrentUser = user;
            ViewBag.LayoutMenuModel = LayoutMenuViewModel;
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}