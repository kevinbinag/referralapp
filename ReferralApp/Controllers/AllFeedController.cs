﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReferralApp.Models;
using ReferralApp.Services;
using ReferralApp.Helpers;
using System.IO;

namespace ReferralApp.Controllers
{
    public class AllFeedController : BaseController
    {
        private ReferralListService _referralListService;
        private ReferralService _referralService;
        private TagService _tagService;

        public AllFeedController()
        {
            _referralListService = new ReferralListService();
            _referralService = new ReferralService();
            _tagService = new TagService();
        }

        public ActionResult AllFeedReferralLists()
        {
            AllFeedReferralListsViewModel viewModel = new AllFeedReferralListsViewModel();
            viewModel.ReferralLists = _referralListService.GetAllActive();
            return View(viewModel);
        }

        public ActionResult AllFeedReferrals()
        {
            AllFeedReferralsViewModel viewModel = new AllFeedReferralsViewModel();
            viewModel.Referrals = _referralService.GetAllActive();
            return View(viewModel);
        }

        public ActionResult AllFeedTags()
        {
            return View();
        }

        public ActionResult AllFeedPeople()
        {
            return View();
        }
    }
}