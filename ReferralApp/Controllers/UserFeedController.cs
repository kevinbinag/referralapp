﻿using ReferralApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReferralApp.Models;
using ReferralApp.Helpers;
using System.IO;

namespace ReferralApp.Controllers
{
    public class UserFeedController : BaseController
    {
        private UserService _userService;
        private ReferralListService _referralListService;
        private ReferralService _referralService;

        public UserFeedController()
        {
            _userService = new UserService();
            _referralListService = new ReferralListService();
            _referralService = new ReferralService();
        }
        // GET: UserFeed
        public ActionResult UserFeedReferrals()
        {
            UserFeedReferralsViewModel viewModel = new UserFeedReferralsViewModel();
            SetBaseUser(viewModel);
            viewModel.ReferralsFollowing = _referralService.GetUserFeedReferrals();
            return View(viewModel);
        }

        public ActionResult UserFeedReferralLists()
        {
            UserFeedReferralListsViewModel viewModel = new UserFeedReferralListsViewModel();
            SetBaseUser(viewModel);
            viewModel.ReferralListsFollowing = _referralListService.GetUserFeedReferralLists();
            return View(viewModel);
        }

        //public ActionResult UserFeedVisited(UserFeedViewModel viewModel)
        //{
        //    //viewModel.UsersFollowing = _userService.GetUsersFollowing();
        //    return View(viewModel);
        //}

        #region Methods
        private void SetBaseUser(BaseViewModel viewModel)
        {
            //viewModel.UserProfileOwner = _userService.GetUserProfileOwnerFromUrl(Request);
            //viewModel.FollowingUser = _leaderFollowerRelationshipService.CurrentUserIsFollowingUser(viewModel.UserProfileOwner.Id);
            var currentUser = ApplicationUserHelper.GetCurrentUser();
            var profilePhoto = currentUser.FilePaths.FirstOrDefault(o => o.FileType == Entities.FileType.UserProfilePhoto);
            if (profilePhoto != null)
            {
                if (!System.IO.File.Exists(Path.Combine(@"C:\Users\kevin_000\Desktop\Projects\ReferralApp\ReferralApp\Content\Images", profilePhoto.FileName)))
                    profilePhoto = null;
            }
            viewModel.CurrentUserProfilePhotoFileName = profilePhoto != null ? profilePhoto.FileName : "";
        }
        #endregion
    }
}