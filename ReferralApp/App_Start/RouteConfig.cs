﻿using ReferralApp.DAL;
using ReferralApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReferralApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            AllFeedRoutes(routes);
            CategoryRoutes(routes);
            TagRoutes(routes);
            UserAnalyticsRoutes(routes);
            UserFeedRoutes(routes);
            UserProfileRoutes(routes);
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }

        private static void AllFeedRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "AllFeedTags",
                url: "all",
                defaults:
                new
                {
                    controller = "AllFeed",
                    action = "AllFeedTags",
                    id = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "AllFeedReferralLists",
                url: "all/lists",
                defaults:
                new
                {
                    controller = "AllFeed",
                    action = "AllFeedReferralLists",
                    id = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "AllFeedReferrals",
                url: "all/linkcodes",
                defaults:
                new
                {
                    controller = "AllFeed",
                    action = "AllFeedReferrals",
                    id = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "AllFeedPeople",
                url: "all/people",
                defaults:
                new
                {
                    controller = "AllFeed",
                    action = "AllFeedPeople",
                    id = UrlParameter.Optional
                }
            );
        }

        private static void CategoryRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "Categories",
                url: "categories",
                defaults: new { controller = "Category", action = "Index", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "CategoryDetails",
                url: "categories/{categoryName}",
                defaults: new { controller = "Category", action = "CategoryDetails", categoryName = "" }
                );
        }

        private static void TagRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "Tags",
                url:"tags",
                defaults: new { controller = "Tag", action = "Index", id = UrlParameter.Optional }
                );
            routes.MapRoute(
                name: "TagDetails",
                url: "tags/{tagName}",
                defaults: new { controller = "Tag", action = "TagDetails", tagName = "" }
                );
        }

        private static void UserAnalyticsRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "Index",
                url: "analytics",
                defaults:
                new
                {
                    controller = "UserAnalytics",
                    action = "Index",
                    id = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "UserAnalyticsReferralLists",
                url: "analytics/lists",
                defaults: 
                new
                {
                    controller = "UserAnalytics",
                    action = "UserAnalyticsReferralLists",
                    id = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "UserAnalyticReferrals",
                url: "analytics/linkcodes",
                defaults: 
                new
                {
                    controller = "UserAnalytics",
                    action = "UserAnalyticsReferrals",
                    id = UrlParameter.Optional
                }
            );
        }

        private static void UserFeedRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "UserFeedReferralLists",
                url: "{controller}",
                defaults: new { controller = "UserFeed", action = "UserFeedReferralLists", id = UrlParameter.Optional },
                constraints: new { controller = new UserFeedConstraint() }
            );

            routes.MapRoute(
                name: "UserFeedReferrals",
                url: "linkcodes",
                defaults: new { controller = "UserFeed", action = "UserFeedReferrals", id = UrlParameter.Optional },
                constraints: new { controller = new UserFeedConstraint() }
            );

            routes.MapRoute(
                name: "UserFeedVisited",
                url: "visited",
                defaults: new { controller = "UserFeed", action = "UserFeedVisited", id = UrlParameter.Optional },
                constraints: new { controller = new UserFeedConstraint() }
            );
        }

        private static void UserProfileRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "UserProfileReferralLists",
                url: "{userName}",
                defaults: new
                {
                    controller = "UserProfile",
                    action = "UserProfileReferralLists",
                    userName = ""
                },
                constraints: new { userName = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "UserProfileReferrals",
                url: "{userName}/linkcodes",
                defaults: new
                {
                    controller = "UserProfile",
                    action = "UserProfileReferrals",
                    userName = ""
                },
                constraints: new { userName = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "UserProfileFollowingReferralLists",
                url: "{userName}/following",
                defaults: new
                {
                    controller = "UserProfile",
                    action = "UserProfileFollowingReferralLists",
                    userName = ""
                },
                constraints: new { userName = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "UserProfileFollowing",
                url: "{userName}/following/linkcodes",
                defaults: new
                {
                    controller = "UserProfile",
                    action = "UserProfileFollowing",
                    userName = ""
                },
                constraints: new { userName = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "UserProfileFollowingTags",
                url: "{userName}/following/tags",
                defaults: new
                {
                    controller = "UserProfile",
                    action = "UserProfileFollowingTags",
                    userName = ""
                },
                constraints: new { userName = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "UserProfileFollowingPeople",
                url: "{userName}/following/people",
                defaults: new
                {
                    controller = "UserProfile",
                    action = "UserProfileFollowingPeople",
                    userName = ""
                },
                constraints: new { userName = new UserNameConstraint() }
            );

            // /username/about
            routes.MapRoute(
                name: "UserProfileAbout",
                url: "{userName}/about",
                defaults: new
                {
                    controller = "UserProfile",
                    action = "UserProfileAbout",
                    userName = ""
                },
                constraints: new { userName = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "UserProfileReferralDetails",
                url: "{userName}/linkcodes/{referralId}",
                defaults: new
                {
                    controller = "UserProfile",
                    action = "UserProfileReferralDetails",
                    userName = "",
                    listId = UrlParameter.Optional
                },
                constraints: new { userName = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "UserProfileListDetails",
                url: "{userName}/lists/{listId}",
                defaults: new
                {
                    controller = "UserProfile",
                    action = "UserProfileListDetails",
                    userName = "",
                    listId = UrlParameter.Optional
                },
                constraints: new { userName = new UserNameConstraint() }
            );
        }

        public class UserNameConstraint : IRouteConstraint
        {
            public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
            {
                // Get the username from the url
                var username = values["userName"].ToString().ToLower();
                // Check for a match (assumes case insensitive)
                return ApplicationUserHelper.UserNameExists(username);
            }
        }

        public class UserFeedConstraint : IRouteConstraint
        {
            public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
            {
                bool matches = false;
                var controller = values["controller"].ToString().ToLower();
                if (controller.Equals("userfeed") && httpContext.Request.IsAuthenticated)
                {
                    matches = true;
                }
                return matches;
            }
        }
    }
}
